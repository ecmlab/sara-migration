﻿using System;
using System.IO;
using System.Windows.Forms;
using HP.HPTRIM.SDK;

namespace AWImport
{
    public partial class FrmStart : Form
    {
        public FrmStart()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            var testdb = new Database();
            if (tbDBID.Text.Trim() == "") return;
            testdb.Id = tbDBID.Text.Trim();
            testdb.Connect();
            testdb.Disconnect();
            testdb.Dispose();

            if (cbcopyRec.Checked)
            {
                string logfile = tbLog.Text + @"\CopyRecs_" + DateTime.Now.ToString(@"yyyyMMddHHmmss") + ".txt";
                using (var sw = new StreamWriter(logfile, true))
                {
                    sw.WriteLine("Copy started at: " + TrimDateTime.Now);
                }

                // Now copy the records
                CopyRecords myRevs = new CopyRecords();
                myRevs.Doit(tbDBID.Text, tbDestID.Text, logfile, tbContainers.Text, tbDocFile.Text, tbPartFile.Text, tbVersionFile.Text, tbaccescontfile.Text);
                using (var sw = new StreamWriter(logfile, true))
                {
                    sw.WriteLine("Copy finished at: " + TrimDateTime.Now);
                }
            }

            if (cbProcContainers.Checked)
            {
                string logfile = tbLog.Text + @"\ProcContainers.txt";
                using (var sw = new StreamWriter(logfile, true))
                {
                    sw.WriteLine("Containers started at: " + TrimDateTime.Now);
                }

                // Now process the Containers.
                using (ProcContainers myConts = new ProcContainers())
                {
                    myConts.Doit(tbDestID.Text, logfile, tbContainers.Text);
                }

                using (var sw = new StreamWriter(logfile, true))
                {
                    sw.WriteLine("Containers finished at: " + TrimDateTime.Now);
                }
            }

            if (cbLoadDocs.Checked)
            {
                string logfile = tbLog.Text + @"\LoadDocs" + DateTime.Now.ToString(@"yyyyMMddHHmmss") + ".txt";
                using (var sw = new StreamWriter(logfile, true))
                {
                    sw.WriteLine("Documents started at: " + TrimDateTime.Now);
                }

                // Now process the documents.
                using (AddDocs doDocs = new AddDocs())
                {
                    doDocs.Doit(tbDestID.Text, logfile, tbDocFile.Text);
                }

                using (var sw = new StreamWriter(logfile, true))
                {
                    sw.WriteLine("Document load finished at: " + TrimDateTime.Now);
                }
            }

            if (cbProcParts.Checked)
            {
                string logfile = tbLog.Text + @"\ProcParts" + DateTime.Now.ToString(@"yyyyMMddHHmmss") + ".txt";
                using (var sw = new StreamWriter(logfile, true))
                {
                    sw.WriteLine("Parts started at: " + TrimDateTime.Now);
                }

                // Now process the parts.
                using (DoParts doParts = new DoParts())
                {
                    doParts.Run(tbDestID.Text, logfile, tbPartFile.Text);
                }

                using (var sw = new StreamWriter(logfile, true))
                {
                    sw.WriteLine("Parts load finished at: " + TrimDateTime.Now);
                }
            }

            if (cbProcVers.Checked)
            {
                string logfile = tbLog.Text + @"\ProcVersions" + DateTime.Now.ToString(@"yyyyMMddHHmmss") + ".txt";
                using (var sw = new StreamWriter(logfile, true))
                {
                    sw.WriteLine("Versions started at: " + TrimDateTime.Now);
                }

                // Now process the versions.
                using (DoVersions doVers = new DoVersions())
                {
                    doVers.Run(tbDestID.Text, logfile, tbVersionFile.Text);
                }

                using (var sw = new StreamWriter(logfile, true))
                {
                    sw.WriteLine("Versions load finished at: " + TrimDateTime.Now);
                }
            }

            if (cbAudits.Checked)
            {
                string logfile = @"C:\temp\ProcAudits_" + DateTime.Now.ToString(@"yyyyMMddHHmmss") + ".txt";
                using (var sw = new StreamWriter(logfile, true))
                {
                    sw.WriteLine("Audits started at: " + TrimDateTime.Now);
                }

                // Now process the Audits
                using (ProcAudits myRevs = new ProcAudits())
                {
                    myRevs.Doit(tbSourceServer.Text, tbSourceDB.Text, tbDestServer.Text, tbDestDB.Text, tbDestID.Text, logfile);
                }

                using (var sw = new StreamWriter(logfile, true))
                {
                    sw.WriteLine("Audits finished at: " + TrimDateTime.Now);
                }
            }

            if (cbExtParts.Checked)
            {
                string logfile = @"C:\temp\ExtractParts_" + DateTime.Now.ToString(@"yyyyMMddHHmmss") + ".txt";
                using (var sw = new StreamWriter(logfile, true))
                {
                    sw.WriteLine("Parts extract started at: " + TrimDateTime.Now);
                }

                // Now process the Parts
                using (ExtractParts myRevs = new ExtractParts())
                {
                    myRevs.Doit(tbDBID.Text, tbDestID.Text, logfile, tbContainers.Text, tbDocFile.Text, tbPartFile.Text, tbVersionFile.Text, tbaccescontfile.Text);
                }

                using (var sw = new StreamWriter(logfile, true))
                {
                    sw.WriteLine("Parts extract finished at: " + TrimDateTime.Now);
                }
            }

            if (cbExtAcc.Checked)
            {
                string logfile = @"C:\temp\ExtractAccess_" + DateTime.Now.ToString(@"yyyyMMddHHmmss") + ".txt";
                using (var sw = new StreamWriter(logfile, true))
                {
                    sw.WriteLine("Access Controls extract started at: " + TrimDateTime.Now);
                }

                // Now extract the Access Controls
                using (CopyAccess myRevs = new CopyAccess())
                {
                    myRevs.Run(tbDBID.Text, logfile, tbaccescontfile.Text);
                }

                using (var sw = new StreamWriter(logfile, true))
                {
                    sw.WriteLine("Access Controls extract finished at: " + TrimDateTime.Now);
                }
            }

            if (cbAccCont.Checked)
            {
                string logfile = @"C:\temp\ProcAccess_" + DateTime.Now.ToString(@"yyyyMMddHHmmss") + ".txt";
                using (var sw = new StreamWriter(logfile, true))
                {
                    sw.WriteLine("Access Controls import started at: " + TrimDateTime.Now);
                }

                // Now extract the Access Controls
                using (DoAccess myRevs = new DoAccess())
                {
                    myRevs.Run(tbDestID.Text, logfile, tbaccescontfile.Text);
                }

                using (var sw = new StreamWriter(logfile, true))
                {
                    sw.WriteLine("Access Controls import finished at: " + TrimDateTime.Now);
                }
            }
            MessageBox.Show(@"All done!");
        }
        private void button4_Click_1(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowDialog();
            tbLog.Text = folderBrowserDialog1.SelectedPath;
            folderBrowserDialog1.Dispose();
        }

        private void btnDocs_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            openFileDialog1.CheckFileExists = false;
            tbDocFile.Text = openFileDialog1.FileName;
            openFileDialog1.Dispose();
        }

        private void btnCont_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            openFileDialog1.CheckFileExists = false;
            tbContainers.Text = openFileDialog1.FileName;
            openFileDialog1.Dispose();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            openFileDialog1.CheckFileExists = false;
            tbPartFile.Text = openFileDialog1.FileName;
            openFileDialog1.Dispose();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            openFileDialog1.CheckFileExists = false;
            tbVersionFile.Text = openFileDialog1.FileName;
            openFileDialog1.Dispose();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            openFileDialog1.CheckFileExists = false;
            tbaccescontfile.Text = openFileDialog1.FileName;
            openFileDialog1.Dispose();
        }
    }
}
