﻿using System;
using System.IO;
using HP.HPTRIM.SDK;

namespace AWImport
{
    internal class CheckEvents : IDisposable
    {
        public object[] Run(object[] auditEvent, string destDbid, long thisNewUri, string logname)
        {
            var destCm = new Database { Id = destDbid };
            destCm.Connect();

            // 0-uri	1-oaObjectType	2-oaObjectUri	3-oaEventDetails	4-oaOtherObjectType	5-oaOtherObjectUri	6-oaSecViolation	7-oaDoneAt	8-oaDoneBy
            // 9-oaDoneByUri	10-oaDoneAtTimezone	11-oaDoneOnMachine	12-oaConnectionIP	13-oaClientIP	14-oaEvent

            // oaObjectType 6 - Record; 9 - Location
            // Increment the URI.
            // auditEvent[0] = Convert.ToInt64(auditEvent[0]);
            auditEvent[0] = thisNewUri;

            // oaObjectType 6 - Record; 9 - Location; etc..
            // Check oaObjectUri

            switch (Convert.ToInt64(auditEvent[1]))
            {
                case 6:
                    {
                        long newRecUri = CheckRec(Convert.ToInt64(auditEvent[2]), destCm, logname);
                        auditEvent[2] = newRecUri;
                        break;
                    }
                case 9:
                    {
                        long newLocUri = CheckLoc(Convert.ToInt64(auditEvent[2]), destCm, logname);
                        auditEvent[2] = newLocUri;
                        break;
                    }
            }
            destCm.Disconnect();
            destCm.Dispose();
            return auditEvent;
        }

        private long CheckLoc(long v, Database destCm, string logout)
        {
            long newLocUri = 0;
            TrimMainObjectSearch locSrch = new TrimMainObjectSearch(destCm, BaseObjectTypes.Location)
            {
                SearchString = "uri:" + v   // Change the search to suit (e.g. look up an additional field).
            };
            if (locSrch.Count == 1)
            {
                foreach (Location thisLoc in locSrch)
                {
                    newLocUri = thisLoc.Uri;
                }
            }
            else if (locSrch.Count == 0)
            {
                Log(logout, "No Location found for URI: " + v);
                newLocUri = v;
            }
            else
            {
                Log(logout, "Multiple Locations found for URI: " + v);
                newLocUri = v;
            }
            return newLocUri;
        }

        private long CheckRec(long v, Database destCm, string logout)
        {
            long newRecUri = 0;
            TrimMainObjectSearch recSrch = new TrimMainObjectSearch(destCm, BaseObjectTypes.Record)
            {
                SearchString = "uri:" + v   // Change the search to suit (e.g. look up an additional field).
            };
            if (recSrch.Count == 1)
            {
                foreach (Record thisRec in recSrch)
                {
                    newRecUri = thisRec.Uri;
                }
            }
            else if (recSrch.Count == 0)
            {
                Log(logout, "No Record found for URI: " + v);
                newRecUri = v;
            }
            else
            {
                Log(logout, "Multiple Records found for URI: " + v);
                newRecUri = v;
            }
            return newRecUri;
        }

        private void Log(string fileName, string message)
        {
            Log(fileName, message, false);
        }
        private void Log(string fileName, string message, bool addTime)
        {
            using (StreamWriter sw = new StreamWriter(Path.Combine(@"C:\temp\", fileName), true))
            {
                if (addTime)
                {
                    message = message + " " + TrimDateTime.Now;
                }
                sw.WriteLine(message);
            }
        }
        public void Dispose()
        {

        }
    }
}
