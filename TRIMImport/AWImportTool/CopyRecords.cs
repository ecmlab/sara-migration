﻿using System;
using System.IO;
using System.Windows.Forms;
using HP.HPTRIM.SDK;
using Exception = System.Exception;

namespace AWImport
{
    class CopyRecords
    {
        public void Doit(string dbid, string destId, string logfile, string containers, string docsFile, string partfile, string versionfile, string accessfile)
        {
            var db = new Database { Id = dbid };
            db.Connect();

            var destDb = new Database { Id = destId };
            destDb.Connect();

            // Find the old SARA uri additional field.
            FieldDefinition destAf =
                (FieldDefinition)destDb.FindTrimObjectByName(BaseObjectTypes.FieldDefinition,
                    "Old TRIM URI");

            // Find the old SARA dbid additional field.
            FieldDefinition destAfdb =
                (FieldDefinition)destDb.FindTrimObjectByName(BaseObjectTypes.FieldDefinition,
                    "Old TRIM DBID");

            // Find the  Migration Provenance additional field.
            FieldDefinition destAfprov =
                (FieldDefinition)destDb.FindTrimObjectByName(BaseObjectTypes.FieldDefinition,
                    "Migration Provenance");

            // Find the old SARA Record Type additional field.
            FieldDefinition destAfRecType =
                (FieldDefinition)destDb.FindTrimObjectByName(BaseObjectTypes.FieldDefinition,
                    "SARA Record Type");

            // Find the old SARA Record Number additional field.
            FieldDefinition destAfRecNum =
                (FieldDefinition)destDb.FindTrimObjectByName(BaseObjectTypes.FieldDefinition,
                    "SARA Record Number");

            // Find the old SARA Record Number additional field.
            FieldDefinition destAfAudHist =
                (FieldDefinition)destDb.FindTrimObjectByName(BaseObjectTypes.FieldDefinition,
                    "SARA Audit History");

            // Find the old SARA TEMP additional field.
            //FieldDefinition destTemp =
            //    (FieldDefinition)destDb.FindTrimObjectByName(BaseObjectTypes.FieldDefinition,
            //        "SARA TEMP");

            // Get a record and try and copy by Record Type...
            TrimMainObjectSearch mySrchRecType = new TrimMainObjectSearch(db, BaseObjectTypes.RecordType);
            mySrchRecType.SetSearchString("all");

            if (mySrchRecType.Count > 0)
            {
                for (var i = 9; i > -1; i--)
                {
                    foreach (RecordType recordType in mySrchRecType)
                    {
                        //if (recordType.UsualBehaviour == RecordBehaviour.Document)
                        //{
                        if (recordType.Name == "Photocopy File" || recordType.Name == "Photocopy Document") continue;
                        if (recordType.Level == i)
                        {
                            TrimMainObjectSearch mySrch = new TrimMainObjectSearch(db, BaseObjectTypes.Record);
                            // mySrch.SetSearchString("all");
                            mySrch.SetSearchString("updated:This Year");
                            mySrch.SetFilterString("type:" + recordType.Uri);
                            mySrch.SetSortString("uri");
                            if (mySrch.Count > 0)
                            {
                                foreach (Record sourceRec in mySrch)
                                {
                                    string thisRectypename;
                                    // Create a new record.
                                    if (sourceRec.RecordType.IsActive)
                                    {
                                        thisRectypename = sourceRec.RecordType.Name;
                                    }
                                    else
                                    {
                                        if (sourceRec.RecordType.UsualBehaviour == RecordBehaviour.Document)
                                        {
                                            thisRectypename = "Legacy Document";
                                        }
                                        else
                                        {
                                            thisRectypename = "Legacy Folder";
                                        }
                                    }

                                    RecordType objRecType = new RecordType(destDb, thisRectypename);
                                    Record destRec = new Record(destDb, objRecType);

                                    // Copy record number.
                                    try
                                    {
                                        if (thisRectypename == "SARA Documents" && sourceRec.Number.StartsWith("D20/"))
                                        {
                                            destRec.LongNumber = "SARA-" + sourceRec.LongNumber;
                                        }
                                        else
                                        {
                                            destRec.LongNumber = sourceRec.LongNumber;
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        if (e.Message.Contains(" has already been used."))
                                        {
                                            try
                                            {
                                                destRec.LongNumber = "SARA-" + sourceRec.LongNumber;
                                            }
                                            catch (Exception exception)
                                            {
                                                using (var sw = new StreamWriter(logfile, true))
                                                {
                                                    sw.WriteLine(
                                                        TrimDateTime.Now +
                                                        "\tError setting record number for Record2: " +
                                                        sourceRec.Number + '\t' + exception.Message);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            using (var sw = new StreamWriter(logfile, true))
                                            {
                                                sw.WriteLine(
                                                    TrimDateTime.Now + "\tError setting record number for Record1: " +
                                                    sourceRec.Number + '\t' + e.Message);
                                            }
                                        }
                                    }

                                    // Copy Title.
                                    try
                                    {
                                        destRec.TypedTitle = sourceRec.TypedTitle;
                                    }
                                    catch (Exception e)
                                    {
                                        using (var sw = new StreamWriter(logfile, true))
                                        {
                                            sw.WriteLine(TrimDateTime.Now + "\tError setting title for Record: " +
                                                         destRec.Number + '\t' + e.Message);
                                        }
                                    }

                                    // Copy Classification
                                    try
                                    {
                                        if (sourceRec.Classification != null)
                                        {
                                            TrimMainObjectSearch mySrchClass =
                                                new TrimMainObjectSearch(destDb, BaseObjectTypes.Classification);
                                            mySrchClass.SetSearchString(
                                                "OldTRIMURI:" + sourceRec.Classification.Uri);
                                            if (mySrchClass.Count == 1)
                                            {
                                                foreach (Classification myClass in mySrchClass)
                                                {
                                                    destRec.Classification = myClass;
                                                }
                                            }
                                            else
                                            {
                                                destRec.Classification = sourceRec.Classification;
                                            }
                                        }
                                        else
                                        {
                                            if (destRec.RecordType.ClassificationMandatory)
                                            {
                                                TrimMainObjectSearch mySrchClass =
                                                    new TrimMainObjectSearch(destDb, BaseObjectTypes.Classification);
                                                mySrchClass.SetSearchString("uri:2019");
                                                if (mySrchClass.Count == 1)
                                                {
                                                    foreach (Classification myClass in mySrchClass)
                                                    {
                                                        destRec.Classification = myClass;
                                                    }
                                                }
                                            }

                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        using (var sw = new StreamWriter(logfile, true))
                                        {
                                            sw.WriteLine(TrimDateTime.Now +
                                                         "\tError setting classification for Record: " +
                                                         destRec.Number + '\t' + e.Message);
                                        }
                                    }

                                    // Copy Retention
                                    try
                                    {
                                        if (sourceRec.RetentionSchedule != null)
                                        {
                                            TrimMainObjectSearch mySrchRet =
                                                new TrimMainObjectSearch(destDb, BaseObjectTypes.Schedule);
                                            mySrchRet.SetSearchString(
                                                "OldTRIMURI:" + sourceRec.RetentionSchedule.Uri);
                                            if (mySrchRet.Count == 1)
                                            {
                                                foreach (Schedule myRet in mySrchRet)
                                                {
                                                    destRec.RetentionSchedule = myRet;
                                                }
                                            }
                                            else
                                            {
                                                mySrchRet = new TrimMainObjectSearch(destDb, BaseObjectTypes.Schedule);
                                                mySrchRet.SetSearchString(
                                                    "name:" + sourceRec.RetentionSchedule.Name + " (2012)");
                                                if (mySrchRet.Count == 1)
                                                {
                                                    foreach (Schedule myRet in mySrchRet)
                                                    {
                                                        destRec.RetentionSchedule = myRet;
                                                    }
                                                }
                                                else
                                                {
                                                    using (var sw = new StreamWriter(logfile, true))
                                                    {
                                                        sw.WriteLine(
                                                            TrimDateTime.Now +
                                                            "\tError setting retention schedule for Record: " +
                                                            destRec.Number + '\t' + sourceRec.RetentionSchedule.Name);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        using (var sw = new StreamWriter(logfile, true))
                                        {
                                            sw.WriteLine(TrimDateTime.Now +
                                                         "\tError setting retention schedule for Record: " +
                                                         destRec.Number + '\t' + e.Message);
                                        }
                                    }

                                    // Record if has versions
                                    if (sourceRec.AllVersions != null)
                                    {
                                        using (var sw = new StreamWriter(versionfile, true))
                                        {
                                            if (sourceRec.Uri != sourceRec.LatestVersion.Uri)
                                            {
                                                sw.WriteLine("START");
                                                //if (sourceRec.CurrentVersion == null)
                                                //{
                                                //    sw.WriteLine(
                                                //        TrimDateTime.Now.ToString() + '\t' + "XXXXXXXX" + '\t' +
                                                //        sourceRec.LatestVersion.Uri);
                                                //}
                                                //else
                                                //{

                                                sw.WriteLine(sourceRec.Uri + '\t' + sourceRec.LatestVersion.Uri);
                                                sw.WriteLine(sourceRec.AllVersions);
                                                //}
                                                sw.WriteLine("END");
                                            }
                                        }
                                    }

                                    // Copy additional Field values.
                                    try
                                    {
                                        destRec = CopyFields(destDb, sourceRec, destRec, logfile);
                                    }
                                    catch (Exception e)
                                    {
                                        using (var sw = new StreamWriter(logfile, true))
                                        {
                                            sw.WriteLine(TrimDateTime.Now +
                                                         "\tError copying additional fields for Record: " +
                                                         sourceRec.Number + '\t' + e.Message);
                                        }
                                    }

                                    // Copy Notes field.
                                    try
                                    {
                                        if (sourceRec.Notes != null)
                                        {
                                            destRec.Notes = sourceRec.Notes;
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        using (var sw = new StreamWriter(logfile, true))
                                        {
                                            sw.WriteLine(TrimDateTime.Now + "\tError setting Notes for Record: " +
                                                         sourceRec.Number + '\t' + e.Message);
                                        }
                                    }

                                    // Set the Container/assignee
                                    if (sourceRec.Container != null)
                                    {
                                        TrimMainObjectSearch mySrchcont =
                                            new TrimMainObjectSearch(destDb, BaseObjectTypes.Record);
                                        mySrchcont.SetSearchString(
                                            "OldTRIMURI:" + sourceRec.Container.Uri);
                                        if (mySrchcont.Count == 1)
                                        {
                                            foreach (Record myCont in mySrchcont)
                                            {
                                                // destRec.Container = myCont;
                                                destRec.SetContainer(myCont, true);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (sourceRec.AssigneeStatus == RecLocSubTypes.AtLocation)
                                        {
                                            FieldDefinition fd =
                                                (FieldDefinition)destDb.FindTrimObjectByName(
                                                    BaseObjectTypes.FieldDefinition,
                                                    "Old TRIM URI");

                                            Location thisLoc = Utils.FindLocByUri(destDb, fd, sourceRec.Assignee.Uri);
                                            if (thisLoc != null)
                                            {
                                                destRec.SetAssignee(thisLoc, null, sourceRec.DateAssigned);
                                            }
                                        }
                                    }

                                    // Set the SARA URI field.
                                    destRec.SetFieldValue(destAf, new UserFieldValue(sourceRec.Uri));

                                    // Set the SARA dbid field.
                                    destRec.SetFieldValue(destAfdb, new UserFieldValue(db.Id));

                                    // Set the old SARA Audit History additional field.
                                    destRec.SetFieldValue(destAfAudHist,
                                        new UserFieldValue(sourceRec.History.Replace("; ", "\r\n")));

                                    // Set the old SARA Record Type additional field.
                                    destRec.SetFieldValue(destAfRecType, new UserFieldValue(sourceRec.RecordType.Name));

                                    // Set the old SARA Record Number additional field.
                                    destRec.SetFieldValue(destAfRecNum, new UserFieldValue(sourceRec.Number));

                                    // Set the Migration Provenance additional field.
                                    destRec.SetFieldValue(destAfprov, new UserFieldValue("SARA"));

                                    // Record the Alt-Containers for later processing.
                                    // 90/5142: Investigation of the Impact of Traditional Turtle and Dugong Hunting on Long Term Viability of Populations\r\n91/8901: Management Strategy for Turtle and Dugong Populations\r\n97/1004: Conservation of Marine Mammals - Dugong (Manatee)
                                    if (sourceRec.AlternativeContainers != string.Empty)
                                    {
                                        using (var sw = new StreamWriter(containers, true))
                                        {
                                            string[] altconts = sourceRec.AlternativeContainers.Split(
                                                new[] { "\r\n" },
                                                StringSplitOptions.RemoveEmptyEntries);
                                            foreach (string altcont in altconts)
                                            {
                                                string[] temp = altcont.Split(':');
                                                Record thisAlt = new Record(db, temp[0]);
                                                sw.WriteLine(TrimDateTime.Now + "\t" + sourceRec.Uri.ToString() + '\t' +
                                                             thisAlt.Uri.ToString() +
                                                             '\t' + "AltCont");
                                            }
                                        }
                                    }

                                    // Extract the electronic objects (Revisions & Renditions)
                                    if (sourceRec.IsElectronic)
                                    {
                                        using (ExtractDocsInfo myDocs = new ExtractDocsInfo())
                                        {
                                            destRec = myDocs.Run(sourceRec, destRec, logfile, docsFile);
                                        }
                                    }

                                    // Do the Creator
                                    if (sourceRec.Creator != null)
                                    {
                                        FieldDefinition fd =
                                            (FieldDefinition)destDb.FindTrimObjectByName(
                                                BaseObjectTypes.FieldDefinition,
                                                "Old TRIM URI");


                                        Location thisLoc = Utils.FindLocByUri(destDb, fd, sourceRec.Creator.Uri);
                                        if (thisLoc != null)
                                        {
                                            destRec.Creator = thisLoc;
                                        }
                                    }
                                    else
                                    {
                                        TrimMainObjectSearch search =
                                            new TrimMainObjectSearch(destDb, BaseObjectTypes.Location);
                                        search.SetSearchString("name: \"SARA Migration 2020\"");
                                        foreach (Location thisCreator in search)
                                        {
                                            if (thisCreator != null)
                                            {
                                                destRec.Creator = thisCreator;
                                            }
                                        }
                                    }

                                    // Do the Home
                                    if (sourceRec.HomeLocation != null)
                                    {
                                        FieldDefinition fd =
                                            (FieldDefinition)destDb.FindTrimObjectByName(
                                                BaseObjectTypes.FieldDefinition,
                                                "Old TRIM URI");


                                        Location thisLoc = Utils.FindLocByUri(destDb, fd, sourceRec.HomeLocation.Uri);
                                        if (thisLoc != null)
                                        {
                                            destRec.SetHomeLocation(thisLoc);
                                        }
                                    }

                                    // Do the Owner
                                    if (sourceRec.OwnerLocation != null)
                                    {
                                        FieldDefinition fd =
                                            (FieldDefinition)destDb.FindTrimObjectByName(
                                                BaseObjectTypes.FieldDefinition,
                                                "Old TRIM URI");


                                        Location thisLoc = Utils.FindLocByUri(destDb, fd, sourceRec.OwnerLocation.Uri);
                                        if (thisLoc != null)
                                        {
                                            destRec.SetOwnerLocation(thisLoc);
                                        }
                                    }

                                    // Do the Author
                                    if (sourceRec.Author != null)
                                    {
                                        FieldDefinition fd =
                                            (FieldDefinition)destDb.FindTrimObjectByName(
                                                BaseObjectTypes.FieldDefinition,
                                                "Old TRIM URI");


                                        Location thisLoc = Utils.FindLocByUri(destDb, fd, sourceRec.Author.Uri);
                                        if (thisLoc != null)
                                        {
                                            destRec.Author = thisLoc;
                                        }
                                    }

                                    // Security
                                    try
                                    {
                                        destRec.Security = sourceRec.Security;
                                    }
                                    catch (Exception e)
                                    {
                                        using (var sw = new StreamWriter(logfile, true))
                                        {
                                            sw.WriteLine(TrimDateTime.Now + "\tError setting security for Record: " +
                                                         destRec.Number +
                                                         '\t' +
                                                         e.Message);
                                        }
                                    }

                                    // Handle the original barcodes.
                                    if (sourceRec.ForeignBarcode != string.Empty)
                                    {
                                        FieldDefinition destAf2 =
                                            (FieldDefinition)destDb.FindTrimObjectByName(
                                                BaseObjectTypes.FieldDefinition,
                                                "SARA Foreign Barcode");
                                        destRec.SetFieldValue(destAf2,
                                            new UserFieldValue(sourceRec.ForeignBarcode));
                                    }

                                    try
                                    {
                                        destRec.ForeignBarcode = sourceRec.Barcode;
                                    }
                                    catch (Exception e)
                                    {
                                        using (var sw = new StreamWriter(logfile, true))
                                        {
                                            sw.WriteLine(TrimDateTime.Now +
                                                         "\tError setting Foreign Barcode for Record: " +
                                                         destRec.Number +
                                                         '\t' + e.Message);
                                        }
                                    }

                                    // Set Dates.

                                    destRec.DateCreated = sourceRec.DateCreated;
                                    destRec.DateRegistered = sourceRec.DateRegistered;
                                    destRec.DateClosed = sourceRec.DateClosed;
                                    destRec.DateDue = sourceRec.DateDue;
                                    destRec.DateFinalized = sourceRec.DateFinalized;
                                    destRec.DateImported = sourceRec.DateImported;
                                    destRec.DateReceived = destRec.DateReceived;

                                    // Save the Record
                                    Application.DoEvents();
                                    try
                                    {
                                        destRec.Save();
                                        using (var sw = new StreamWriter(logfile, true))
                                        {
                                            sw.WriteLine(TrimDateTime.Now + "\tCopied Record: " + sourceRec.Number);
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        using (var sw = new StreamWriter(logfile, true))
                                        {
                                            sw.WriteLine(TrimDateTime.Now + "\tError copying record1: " +
                                                         sourceRec.Number + '\t' +
                                                         e.Message);
                                            //if (e.Message.Contains("Series record"))
                                            //{
                                            //    // stop here 
                                            //    MessageBox.Show("Here");
                                            //}
                                        }
                                    }

                                    //// Clean up the document.
                                    //if (sourceRec.IsElectronic)
                                    //{
                                    //    string tempUdf = destRec.GetFieldValueAsString(destTemp,
                                    //        StringDisplayType.Default, false);
                                    //    if (tempUdf != string.Empty)
                                    //    {
                                    //        try
                                    //        {
                                    //            if (File.Exists(tempUdf))
                                    //            {
                                    //                File.Delete(tempUdf);

                                    //            }
                                    //            else
                                    //            {
                                    //                using (var sw = new StreamWriter(logfile, true))
                                    //                {
                                    //                    sw.WriteLine(
                                    //                        TrimDateTime.Now + "\tFile not found to delete: " +
                                    //                        tempUdf);
                                    //                }
                                    //            }
                                    //        }
                                    //        catch (Exception e)
                                    //        {
                                    //            using (var sw = new StreamWriter(logfile, true))
                                    //            {
                                    //                sw.WriteLine(TrimDateTime.Now + "\tError deleting document: " +
                                    //                             tempUdf + '\t' + e.Message);
                                    //            }
                                    //        }
                                    //    }
                                    //}
                                }
                            }
                        }
                        //}
                    }
                }
            }
            db.Disconnect();
            db.Dispose();
            destDb.Disconnect();
            destDb.Dispose();
        }

        private static Record CopyFields(Database destDb, Record sourceRec, Record destRec, string logfile)
        {
            // Copy additional fields
            using (CopyAddFields addFields = new CopyAddFields())
            {
                destRec = addFields.Run(destDb, sourceRec, destRec, logfile);
            }
            return destRec;
        }

        //private static Record CopyAccess(Database destDb, Record sourceRec, Record destRec, string logfile, string accessfile)
        //{
        //    // Copy access controls.
        //    using (CopyAccess myAccess = new CopyAccess())
        //    {
        //        destRec = myAccess.Run(destDb, sourceRec, destRec, logfile, accessfile);
        //    }
        //    return destRec;
        //}
    }
}
