﻿using System;
using System.IO;
using System.Windows.Forms;
using HP.HPTRIM.SDK;

namespace AWImport
{
    public class DoAccess : IDisposable
    {
        public void Dispose()
        {

        }

        public void Run(string destId, string logfile, string access)
        {
            var destDb = new Database { Id = destId };
            destDb.Connect();

            FieldDefinition fd =
                (FieldDefinition)destDb.FindTrimObjectByName(
                    BaseObjectTypes.FieldDefinition,
                    "Old TRIM URI");

            using (StreamReader sr = new StreamReader(access))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    if (line != string.Empty)
                    {
                        string[] bits = line.Split('\t');
                        // 0 - sourceRec.Uri, 1 =  acc enum, 2 - loc name, 3 - accessLocation.Uri);

                        TrimMainObjectSearch recsrch = new TrimMainObjectSearch(destDb, BaseObjectTypes.Record);
                        //recsrch.SetSearchString("number:" + bits[0]);
                        recsrch.SetSearchString(
                            "OldTRIMURI:" + bits[0]);

                        if (recsrch.Count == 1)
                        {
                            foreach (Record thisRecord in recsrch)
                            {
                                try
                                {
                                    LocationList thisList = new LocationList();
                                    int thisenum = Convert.ToInt32(bits[1]);
                                    TrimAccessControlList recordAcl = thisRecord.AccessControlList;
                                    foreach (Location accessLocation in recordAcl.GetAccessLocations(thisenum))
                                    {
                                        thisList.Add(accessLocation);
                                    }
                                    Location thisLoc = Utils.FindLocByUri(destDb, fd, Convert.ToInt32(bits[3]));
                                    if (thisLoc != null)
                                    {
                                        thisList.Add(thisLoc);
                                    }
                                                                        
                                    recordAcl.SetAccessLocations(thisenum, thisList);
                                    thisRecord.AccessControlList = recordAcl;
                                    //private static TrimAccessControlList GiveAccess(TrimAccessControlList acl, int right, Location access)
                                    //{
                                    //    LocationList ll = acl.GetAccessLocations(right);
                                    //    ll.Add(access);
                                    //    acl.SetAccessLocations(right, ll);
                                    //    return acl;
                                    //}

                                    //thisRecord.AccessControlList.SetCurrentAccessControlSettings(thisenum, AccessControlSettings.PrivateOrContainer);
                                    //thisRecord.AccessControlList.SetCurrentAccessControlSettings(thisenum, AccessControlSettings.Private);
                                    //thisRecord.AccessControlList.SetAccessLocations(thisenum, thisList);
                                }
                                catch (Exception e)
                                {
                                    using (var sw = new StreamWriter(logfile, true))
                                    {
                                        sw.WriteLine(TrimDateTime.Now + "\tError adding location for: " + '\t' + thisRecord.Number + '\t' + e.Message);
                                    }
                                }
                                // Save the Record
                                Application.DoEvents();
                                try
                                {
                                    thisRecord.Save();
                                    using (var sw = new StreamWriter(logfile, true))
                                    {
                                        sw.WriteLine(TrimDateTime.Now + "\tAccess Control set for: " + thisRecord.Number);
                                    }
                                }
                                catch (Exception e)
                                {
                                    using (var sw = new StreamWriter(logfile, true))
                                    {
                                        sw.WriteLine(TrimDateTime.Now + "\tError setting Access Control for: \t" + thisRecord.Number + '\t' + e.Message);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            destDb.Disconnect();
            destDb.Dispose();
        }
        // enum  	RecordAccess {
        //RecordAccess.ViewDocument = 1, RecordAccess.ViewRecord = 2, RecordAccess.UpdateDocument = 3, RecordAccess.UpdateMetadata = 4,
        //RecordAccess.ModifyAccess = 5, RecordAccess.DestroyRecord = 6, RecordAccess.AddContents = 7
    }
}