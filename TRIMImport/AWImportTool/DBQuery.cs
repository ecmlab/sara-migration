﻿
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using HP.HPTRIM.SDK;

namespace AWImport
{
    internal class QueryDb : IDisposable
    {
        private const string _ConnectionString = "Data Source={0};Initial Catalog={1};Integrated Security=true;Persist Security Info=False";
        private const string CountString = "SELECT COUNT(*) FROM {0}";

        private SqlConnection _connection;

        private string _serverName;
        public string ServerName
        {
            get => _serverName;
            set => _serverName = value;
        }

        private string DatabaseName { get; set; }

        private string ConnectionString
        {
            get
            {
                if (DatabaseName == "" || _serverName == "")
                {
                    return "";
                }
                else
                {
                    return string.Format(_ConnectionString, _serverName, DatabaseName);
                }
            }
        }

        public QueryDb(string svrName, string dbName)
        {
            _serverName = svrName;
            DatabaseName = dbName;
        }

        public void Dispose()
        {
            if (_connection != null)
            {
                try
                {
                    _connection.Close();
                    _connection.Dispose();
                    _connection = null;
                }
                catch (Exception ex)
                {
                    // TODO: Error Handling
                }
            }
        }

        private string GetCountQuery(string tableName)
        {
            return string.Format(CountString, tableName);
        }

        private bool Connect()
        {
            if (ConnectionString == "")
            {
                return false;
            }
            else
            {
                try
                {
                    _connection = new SqlConnection(string.Format(_ConnectionString, _serverName, DatabaseName));
                    _connection.Open();
                    return true;
                }
                catch (Exception ex)
                {
                    Log("Status.txt", ex.Message, true);
                    if (IsConnected())
                    {
                        _connection.Close();
                        _connection.Dispose();
                        _connection = null;
                    }
                    return false;
                }
            }
        }

        private bool IsConnected()
        {
            return (_connection != null);
        }

        public List<object[]> Query(string sqlQuery)
        {
            List<object[]> rows = new List<object[]>();

            if (Connect())
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand(sqlQuery, _connection))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    object[] columns = new object[reader.FieldCount];
                                    if (reader.GetValues(columns) > 0)
                                    {
                                        rows.Add(columns);
                                    }
                                }
                                reader.Close();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // TODO: Exception handling
                }
            }
            return rows;
        }

        public long Count(string table)
        {
            if (Connect())
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand(GetCountQuery(table), _connection))
                    {
                        return (long)cmd.ExecuteScalar();
                    }
                }
                catch (Exception ex)
                {
                    // TODO: Exception handling
                }
            }
            return 0;
        }
        private void Log(string fileName, string message)
        {
            Log(fileName, message, false);
        }

        private void Log(string fileName, string message, bool addTime)
        {
            using (StreamWriter sw = new StreamWriter(Path.Combine(@"C:\temp\", fileName), true))
            {
                if (addTime)
                {
                    message = message + " " + TrimDateTime.Now;
                }
                sw.WriteLine(TrimDateTime.Now + "\t" + message);
            }
        }
    }
}