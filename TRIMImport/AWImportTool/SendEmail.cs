﻿using System;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Windows.Forms;
using HP.HPTRIM.SDK;

namespace AWImport
{
    class MySendEmail
    {
        internal static void Run(string msgToSend, string msgAtt, string logfile)
        {
            var smtpServer = "";
            var smtpTo = "";
            var smtpFrom = "";

            using (var config = File.OpenText("Settings.txt"))
            {
                string sLineIn;
                while ((sLineIn = config.ReadLine()) != null)
                {
                    string[] mySplit = sLineIn.Split(':');
                    if (mySplit[0] == "SMTPServer")
                    {
                        smtpServer = mySplit[1];
                    }
                    if (mySplit[0] == "MailTo")
                    {
                        smtpTo = mySplit[1];
                    }
                    if (mySplit[0] == "MailFrom")
                    {
                        smtpFrom = mySplit[1];
                    }
                }
            }

            if (smtpServer != "none")
            {
                try
                {
                    var email = new MailMessage(smtpFrom, smtpTo);
                    var data = new Attachment(msgAtt, MediaTypeNames.Application.Octet);
                    var smtp = new SmtpClient(smtpServer);
                    email.Subject = "Migration status report.";
                    email.Body = msgToSend;
                    email.Attachments.Add(data);
                    smtp.Send(email);
                    email.Dispose();
                    data.Dispose();
                }
                catch (Exception ex)
                {
                    using (var sw = new StreamWriter(logfile + @"\Mail_Issue.txt", true))
                    {
                        string myMsg = ex.Message;
                        sw.WriteLine("Email sending failed at: " + TrimDateTime.Now + "\r\n" + myMsg + "\r\n\r\n");
                        Application.DoEvents();
                    }
                }

            }
        }
    }
}
