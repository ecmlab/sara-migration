﻿using System;
using System.IO;
using System.Windows.Forms;
using HP.HPTRIM.SDK;
using Exception = System.Exception;

namespace AWImport
{
    class ExtractParts : IDisposable
    {
        public void Doit(string dbid, string destId, string logfile, string containers, string docsFile, string partfile, string versionfile, string accessfile)
        {
            var db = new Database { Id = dbid };
            db.Connect();

            var destDb = new Database { Id = destId };
            destDb.Connect();

            // Get a record and try and copy by Record Type...
            TrimMainObjectSearch mySrchRecType = new TrimMainObjectSearch(db, BaseObjectTypes.RecordType);
            mySrchRecType.SetSearchString("all");

            if (mySrchRecType.Count > 0)
            {
                for (var i = 9; i > -1; i--)
                {
                    foreach (RecordType recordType in mySrchRecType)
                    {
                        if (recordType.Name == "Photocopy File" || recordType.Name == "Photocopy Document") continue;
                        if (recordType.Level == i)
                        {
                            TrimMainObjectSearch mySrch = new TrimMainObjectSearch(db, BaseObjectTypes.Record);
                            // mySrch.SetSearchString("all");
                            mySrch.SetSearchString("updated:This Year");
                            mySrch.SetFilterString("type:" + recordType.Uri);
                            mySrch.SetSortString("uri");
                            if (mySrch.Count > 0)
                            {
                                foreach (Record sourceRec in mySrch)
                                {
                                    // Record if has parts
                                    if ((sourceRec.IsInPartSeries) && (sourceRec.NextPartRecord != null))
                                    {
                                        //using (var sw = new StreamWriter(partfile, true))
                                        //{
                                        //    sw.WriteLine("START");
                                        //}
                                        using (var sw = new StreamWriter(partfile, true))
                                        {
                                            sw.WriteLine(sourceRec.Uri.ToString() + '\t' + sourceRec.NextPartRecord.Uri); //    sourceRec.NextPartRecord.Uri);
                                            // sw.WriteLine(sourceRec.AllParts);
                                        }
                                        //using (var sw = new StreamWriter(partfile, true))
                                        //{
                                        //    sw.WriteLine("END");
                                        //}
                                    }
                                }
                            }
                        }
                    }
                }
            }
            db.Disconnect();
            db.Dispose();
            destDb.Disconnect();
            destDb.Dispose();
        }

        public void Dispose()
        {
            
        }
    }
}
