﻿using System;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using Microsoft.Win32;

namespace AWImport
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>

        [STAThread]
        static void DoSomething()
        {
            // Main part of program.
            const string trimInstallDir = @"H:\APPS\Program Files\HP TRIM";
            var temp = Environment.GetEnvironmentVariable("PATH") + ";" + trimInstallDir;
            Environment.SetEnvironmentVariable("PATH", temp);
            Application.Run(new FrmStart());
        }

        [STAThread]
        static void Main()
        {
            string installDir = FindInstalledTrimPath();
            string pathToSdkDll = Path.Combine(installDir, "HP.HPTRIM.SDK.dll");
            AppDomain.CurrentDomain.AssemblyResolve +=
                (sender, e) => AssemblyResolveEventHandler(sender, e, pathToSdkDll);
            // ensure that no SDK code is loaded in the method that configures the assembly resolver otherwise the .Net runtime will attempt
            // to load the SDK prior to the resolver being configured.
            DoSomething();
        }

        // ReSharper disable once UnusedParameter.Local
        private static Assembly AssemblyResolveEventHandler(object sender, ResolveEventArgs args, string pathToSdkDll)
        {
            if (args.Name.Contains("HP.HPTRIM.SDK"))
            {
                Assembly assembly = Assembly.LoadFrom(pathToSdkDll);
                return assembly;
            }

            return null;
        }

        private static string FindInstalledTrimPath()
        {
            RegistryKey[] registryHives =
            {
                Registry.LocalMachine,
                Registry.CurrentUser
            };

            string[] registryPaths =
            {
                @"SOFTWARE\Micro Focus\Content Manager\MSISettings",
                @"SOFTWARE\Hewlett-Packard\HP TRIM\MSISettings"
            };

            string INSTALLDIR = "INSTALLDIR";

            foreach (var hive in registryHives)
            {
                foreach (var regPath in registryPaths)
                {
                    RegistryKey msiSettingsKey = hive.OpenSubKey(regPath);
                    if (msiSettingsKey != null)
                    {
                        string installDir = msiSettingsKey.GetValue(INSTALLDIR) as string;
                        if (!string.IsNullOrEmpty(installDir))
                        {
                            return installDir;
                        }
                    }
                }
            }

            return null;
        }
    }
}
