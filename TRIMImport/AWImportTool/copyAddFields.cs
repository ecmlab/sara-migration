﻿using System;
using System.IO;
using HP.HPTRIM.SDK;

namespace AWImport
{
    public class CopyAddFields : IDisposable
    {
        public Record Run(Database destDb, Record sourceRec, Record destRec, string logfile)
        {
            FieldDefinitionList udfsSource = sourceRec.RecordType.UserFields;

            foreach (FieldDefinition udf in udfsSource)
            {
                UserFieldValue ufvCurrent = null;
                switch (udf.Format)
                {
                    case UserFieldFormats.Datetime:
                        if (sourceRec.GetFieldValue(udf).AsDate() != null)
                        {
                            TrimDateTime tdta = new TrimDateTime(sourceRec.GetFieldValue(udf).AsDate());
                            ufvCurrent = new UserFieldValue(tdta);
                        }
                        break;
                    case UserFieldFormats.String:
                    case UserFieldFormats.Text:
                    case UserFieldFormats.Xml:
                        ufvCurrent = new UserFieldValue(sourceRec.GetFieldValue(udf).ToString());
                        break;
                }

                if (sourceRec.RecordType.Name == "GRR Document" || sourceRec.RecordType.Name == "SARA Documents")
                {
                    if (udf.Name == "Format")
                    {
                        ufvCurrent = new UserFieldValue("Electronic Only");
                    }
                }

                if (ufvCurrent != null)
                {
                    try
                    {
                        FieldDefinition destAf = (FieldDefinition)destDb.FindTrimObjectByName(BaseObjectTypes.FieldDefinition, udf.Name);
                        destRec.SetFieldValue(destAf, ufvCurrent);
                    }
                    catch (Exception e)
                    {
                        using (var sw = new StreamWriter(logfile, true))
                        {
                            sw.WriteLine(TrimDateTime.Now + "\tError processing UDF: " + udf.Name + "Message: " + e.Message);
                        }
                    }
                }
            }
            return destRec;
        }

        public void Dispose()
        {

        }
    }
}