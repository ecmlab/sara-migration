﻿using System;
using System.IO;
using System.Windows.Forms;
using HP.HPTRIM.SDK;

namespace AWImport
{
    public class DoVersions : IDisposable
    {
        public void Dispose()
        {
            
        }

        public void Run(string destId, string logfile, string version)
        {
            var destDb = new Database { Id = destId };
            destDb.Connect();

            using (StreamReader sr = new StreamReader(version))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] bits = line.Split('\t');
                    // 0 - this version, 1 - next version

                    TrimMainObjectSearch recsrch =
                        new TrimMainObjectSearch(destDb, BaseObjectTypes.Record);
                    recsrch.SetSearchString("number:" + bits[0]);
                    recsrch.SetSearchString(
                        "OldTRIMURI:" + bits[0]);

                    if (recsrch.Count == 1)
                    {
                        foreach (Record thisVers in recsrch)
                        {
                            TrimMainObjectSearch nextVers = new TrimMainObjectSearch(destDb, BaseObjectTypes.Record);
                            nextVers.SetSearchString("OldTRIMURI:" + bits[1]);
                            if (nextVers.Count == 1)
                            {
                                foreach (Record nextVer in nextVers)
                                {
                                    thisVers.AttachRelationship(nextVer, RecordRelationshipType.IsVersion);
                                }
                            }
                            // Save the Record
                            Application.DoEvents();
                            try
                            {
                                thisVers.Save();
                                using (var sw = new StreamWriter(logfile, true))
                                {
                                    sw.WriteLine(TrimDateTime.Now + "\tNext Version set for: " + thisVers.Number);
                                }
                            }
                            catch (Exception e)
                            {
                                using (var sw = new StreamWriter(logfile, true))
                                {
                                    sw.WriteLine(TrimDateTime.Now + "\tError setting Next Version for: " + thisVers.Number + '\t' + e.Message);
                                }
                            }
                        }
                    }
                }
            }
            destDb.Disconnect();
            destDb.Dispose();
        }
    }
}