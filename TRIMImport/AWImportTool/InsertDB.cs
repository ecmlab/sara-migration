﻿
using System;
using System.Data.SqlClient;
using System.IO;
using HP.HPTRIM.SDK;

namespace AWImport
{
    internal class InsertDb : IDisposable
    {
        private const string _ConnectionString = "Data Source={0};Initial Catalog={1};Integrated Security=true;Persist Security Info=False";

        private SqlConnection _connection;

        public string ServerName { get; set; }

        private string DatabaseName { get; set; }

        private string ConnectionString
        {
            get
            {
                if (DatabaseName == "" || ServerName == "")
                {
                    return "";
                }
                else
                {
                    return string.Format(_ConnectionString, ServerName, DatabaseName);
                }
            }
        }

        public InsertDb(string svrName, string dbName)
        {
            ServerName = svrName;
            DatabaseName = dbName;
        }

        public void Dispose()
        {
            if (_connection != null)
            {
                try
                {
                    _connection.Close();
                    _connection.Dispose();
                    _connection = null;
                }
                catch (Exception ex)
                {
                    // TODO: Error Handling
                }
            }
        }

        private bool Connect()
        {
            if (ConnectionString == "")
            {
                return false;
            }
            else
            {
                try
                {
                    _connection = new SqlConnection(string.Format(_ConnectionString, ServerName, DatabaseName));
                    _connection.Open();
                    return true;
                }
                catch (Exception ex)
                {
                    Log("Status.txt", ex.Message, true);
                    if (IsConnected())
                    {
                        _connection.Close();
                        _connection.Dispose();
                        _connection = null;
                    }
                    return false;
                }
            }
        }

        private bool IsConnected()
        {
            return (_connection != null);
        }

        public bool Insert(object[] InRow)
        {
            if (Connect())
            {
                try
                {
                    // _connection.Open();
                    using (SqlCommand command = _connection.CreateCommand())
                    {
                        command.CommandText =
                            "INSERT INTO TSONLINEAU (uri, oaObjectType, oaObjectUri, oaEventDetails, oaOtherObjectType, oaOtherObjectUri, oaSecViolation, oaDoneAt, oaDoneBy, oaDoneByUri, oaDoneAtTimezone, oaDoneOnMachine, oaConnectionIP, oaClientIP, oaEvent) VALUES (@param1,@param2,@param3,@param4,@param5,@param6,@param7,@param8,@param9,@param10,@param11,@param12,@param13,@param14,@param15)";

                        command.Parameters.AddWithValue("@param1", InRow[0]);
                        command.Parameters.AddWithValue("@param2", InRow[1]);
                        command.Parameters.AddWithValue("@param3", InRow[2]);
                        command.Parameters.AddWithValue("@param4", InRow[3]);
                        command.Parameters.AddWithValue("@param5", InRow[4]);
                        command.Parameters.AddWithValue("@param6", InRow[5]);
                        command.Parameters.AddWithValue("@param7", InRow[6]);
                        command.Parameters.AddWithValue("@param8", InRow[7]);
                        command.Parameters.AddWithValue("@param9", InRow[8]);
                        command.Parameters.AddWithValue("@param10", InRow[9]);
                        command.Parameters.AddWithValue("@param11", InRow[10]);
                        command.Parameters.AddWithValue("@param12", InRow[11]);
                        command.Parameters.AddWithValue("@param13", InRow[12]);
                        command.Parameters.AddWithValue("@param14", InRow[13]);
                        command.Parameters.AddWithValue("@param15", InRow[14]);
                        try
                        {
                            command.ExecuteNonQuery();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                            throw;
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
            return true;
        }

        private void Log(string fileName, string message)
        {
            Log(fileName, message, false);
        }

        private void Log(string fileName, string message, bool addTime)
        {
            using (StreamWriter sw = new StreamWriter(Path.Combine(@"C:\temp\", fileName), true))
            {
                if (addTime)
                {
                    message = message + " " + TrimDateTime.Now;
                }
                sw.WriteLine(message);
            }
        }
    }
}