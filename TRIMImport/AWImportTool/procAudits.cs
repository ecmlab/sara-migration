﻿using System;
using System.Collections.Generic;

namespace AWImport
{
    class ProcAudits : IDisposable
    {
        public void Doit(string sourceServ, string sourceDb, string destServ, string destDb, string destDbid, string logfile)
        {
            // Get all the Audit Events
            List<object[]> auditEvents;

            using (QueryDb sql = new QueryDb(sourceServ, sourceDb))
            {
                auditEvents = sql.Query("SELECT * FROM TSONLINEAU WHERE URI > 0");
            }

            List<object[]> maxEvent;
            using (QueryDb sql = new QueryDb(destServ, destDb))
            { 
                maxEvent = sql.Query("SELECT MAX(URI) FROM TSONLINEAU");
            }

            long maxUri = Convert.ToInt64(maxEvent[0][0].ToString());
            long startUri = maxUri + 1000000;
            foreach (object[] auditEvent in auditEvents)
            {
                // Modify the entries in this line to suit new database.
                using (CheckEvents chkThis = new CheckEvents())
                {
                    object[] auditEventNew =  chkThis.Run(auditEvent, destDbid, startUri, logfile);
                    using (InsertDb sql = new InsertDb(destServ, destDb))
                    {
                        bool insResult = sql.Insert(auditEventNew);
                    }
                    startUri++;
                }
            }
        }

        public void Dispose()
        {
            
        }
    }
}
