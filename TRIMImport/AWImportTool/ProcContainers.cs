﻿using System;
using System.IO;
using System.Windows.Forms;
using HP.HPTRIM.SDK;

namespace AWImport
{
    public class ProcContainers : IDisposable
    {
        public void Doit(string destId, string logfile, string containers)
        {
            var destDb = new Database { Id = destId };
            destDb.Connect();

            using (StreamReader sr = new StreamReader(containers))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] bits = line.Split('\t');
                    // 0 - Item, 1 - Container, 2 - Type

                    

                    TrimMainObjectSearch recsrch =
                        new TrimMainObjectSearch(destDb, BaseObjectTypes.Record);
                    recsrch.SetSearchString("number:" + bits[0]);
                    recsrch.SetSearchString(
                        "OldTRIMURI:" + bits[0]);


                    if (recsrch.Count == 1)
                    {
                        foreach (Record record in recsrch)
                        {
                            TrimMainObjectSearch reccont = new TrimMainObjectSearch(destDb, BaseObjectTypes.Record);
                            reccont.SetSearchString("OldTRIMURI:" + bits[1]);
                            if (reccont.Count == 1)
                            {
                                foreach (Record fndcont in reccont)
                                {
                                    if (bits[2] == ("Cont"))
                                    {
                                        record.SetContainer(fndcont, true);
                                    }
                                    else if (bits[2] == "AltCont")
                                    {
                                        record.AttachRelationship(fndcont, RecordRelationshipType.IsAltIn);
                                    }
                                }
                            }
                            // Save the Record
                            Application.DoEvents();
                            try
                            {
                                record.Save();
                                using (var sw = new StreamWriter(logfile, true))
                                {
                                    sw.WriteLine(TrimDateTime.Now + "\tContainer set for: " + record.Number);
                                }
                            }
                            catch (Exception e)
                            {
                                using (var sw = new StreamWriter(logfile, true))
                                {
                                    sw.WriteLine(TrimDateTime.Now + "\tError setting Container for: " + record.Number + '\t' + e.Message);
                                }
                            }
                        }
                    }
                }
            }
            destDb.Disconnect();
            destDb.Dispose();
        }

        public void Dispose()
        {

        }
    }
}