﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using HP.HPTRIM.SDK;

namespace AWImport
{
    class ExtractDocsInfo : IDisposable
    {
        public Record Run(Record recIn, Record recOut, string logfile, string docsFile)
        {
            Application.DoEvents();
            using (var sw = new StreamWriter(logfile, true))
            {
                sw.WriteLine(TrimDateTime.Now + "\tProcessing document: " + recIn.Number);
            }

            string result = string.Empty;

            if (recIn.RevisionCount == 1)
            {
                ElectronicStore newStore = null;

                TrimMainObjectSearch findStores =
                    new TrimMainObjectSearch(recOut.Database, BaseObjectTypes.ElectronicStore) { SearchString = "All" };
                foreach (ElectronicStore thisStore in findStores)
                {
                    if (thisStore.Uri == recIn.EStore.Uri)
                    {
                        newStore = thisStore;
                    }
                }

                if (newStore != null)
                {
                    string revStoreId2 = recIn.EStoreId;
                    InputDocument myDoc = new InputDocument(newStore, revStoreId2, recIn.DocumentSize, recIn.Extension);
                    //InputDocument myDoc = new InputDocument(result);
                    recOut.SetDocument(myDoc, false, false, String.Empty);
                }

                //}
            }

            if (recIn.RevisionCount > 1)
            {
                StringBuilder outBuilder = new StringBuilder();
                // Get each revision details.
                foreach (TrimChildObject recInChildRevision in recIn.ChildRevisions)
                {
                    string thisRev = recInChildRevision.GetPropertyAsString(
                        PropertyIds.RecordRevisionRevisionNumber,
                        StringDisplayType.Default, false);
                    TrimObject revStore =
                        recInChildRevision.GetPropertyTrimObject(PropertyIds.RecordRevisionEStore);
                    string revStoreId = recInChildRevision.GetPropertyAsString(
                        PropertyIds.RecordRevisionEStoreId,
                        StringDisplayType.Default, false);
                    string sStore = revStore.GetPropertyAsString(PropertyIds.ElectronicStorePath,
                        StringDisplayType.Default, false);
                    string fullDocName =
                        (sStore + revStoreId.Replace("????", "").Replace("+", @"\")).Replace(@"\\", @"\");
                    outBuilder.Insert(0, "Rev" + "\t" + recIn.Number + '\t' + recIn.Uri + '\t' + thisRev + '\t' + fullDocName + '\t' + revStore.Uri + '\t' + revStoreId + '\t' + recInChildRevision.GetPropertyAsString(PropertyIds.RecordRevisionBytes, StringDisplayType.Default, false) + '\t' + recInChildRevision.GetPropertyAsString(PropertyIds.RecordRevisionExtension, StringDisplayType.Default, false) + "\r\n");
                }

                // Get the main document details.
                string thisRev2 = recIn.RevisionNumber.ToString();
                TrimObject revStore2 = recIn.EStore;
                string revStoreId2 = recIn.EStoreId;
                string sStore2 = revStore2.GetPropertyAsString(PropertyIds.ElectronicStorePath,
                    StringDisplayType.Default, false);
                string fullDocName2 =
                    (sStore2 + revStoreId2.Replace("????", "").Replace("+", @"\")).Replace(@"\\", @"\");
                outBuilder.AppendLine("Main" + "\t" + recIn.Number + '\t' + recIn.Uri + '\t' + thisRev2 + '\t' + fullDocName2 + '\t' + recIn.ESource + '\t' + revStore2.Uri + '\t' + revStoreId2 + '\t' + recIn.DocumentSize + '\t' + recIn.Extension + "\r\n");

                using (var sw = new StreamWriter(docsFile, true))
                {
                    sw.WriteLine(outBuilder);
                }
            }

            if (recIn.RenditionCount > 0)
            {
                for (uint i = 0; i < recIn.RenditionCount; i++)
                {
                    string thisDesc = recIn.ChildRenditions.getItem(0).Description;
                    TrimObject revStore2 = recIn.ChildRenditions.getItem(i).EStore;
                    string revStoreId2 = recIn.ChildRenditions.getItem(i).EStoreId;
                    string sStore2 = revStore2.GetPropertyAsString(PropertyIds.ElectronicStorePath,
                        StringDisplayType.Default, false);
                    string fullDocName =
                        (sStore2 + revStoreId2.Replace("????", "").Replace("+", @"\")).Replace(@"\\", @"\");

                    using (var sw = new StreamWriter(docsFile, true))
                    {
                        sw.WriteLine("Rend" + "\t" + recIn.Number + '\t' + recIn.Uri + '\t' + thisDesc + '\t' +
                                     fullDocName + '\t' + revStore2.Uri + '\t' + revStoreId2 + '\t' + recIn.ChildRenditions.getItem(i).Bytes + '\t' + recIn.ChildRenditions.getItem(i).Extension);
                    }
                }
            }
            return recOut;
        }
        public void Dispose()
        {

        }
    }
}
