﻿namespace AWImport
{
    partial class FrmStart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbStart = new System.Windows.Forms.TextBox();
            this.tbStop = new System.Windows.Forms.TextBox();
            this.tbDBID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnLog = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tbLog = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.cbcopyRec = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbDestID = new System.Windows.Forms.TextBox();
            this.cbAudits = new System.Windows.Forms.CheckBox();
            this.tbSourceServer = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbSourceDB = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tbDestDB = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbDestServer = new System.Windows.Forms.TextBox();
            this.btnCont = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.tbContainers = new System.Windows.Forms.TextBox();
            this.cbProcContainers = new System.Windows.Forms.CheckBox();
            this.btnDocs = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.tbDocFile = new System.Windows.Forms.TextBox();
            this.cbLoadDocs = new System.Windows.Forms.CheckBox();
            this.cbProcParts = new System.Windows.Forms.CheckBox();
            this.cbProcVers = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.tbPartFile = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.tbVersionFile = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.tbaccescontfile = new System.Windows.Forms.TextBox();
            this.cbExtParts = new System.Windows.Forms.CheckBox();
            this.cbExtAcc = new System.Windows.Forms.CheckBox();
            this.cbAccCont = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(193, 488);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(72, 41);
            this.button1.TabIndex = 0;
            this.button1.Text = "GO!";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(86, 535);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Start";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(86, 563);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Stop";
            // 
            // tbStart
            // 
            this.tbStart.Location = new System.Drawing.Point(128, 535);
            this.tbStart.Name = "tbStart";
            this.tbStart.Size = new System.Drawing.Size(245, 20);
            this.tbStart.TabIndex = 3;
            // 
            // tbStop
            // 
            this.tbStop.Location = new System.Drawing.Point(128, 563);
            this.tbStop.Name = "tbStop";
            this.tbStop.Size = new System.Drawing.Size(245, 20);
            this.tbStop.TabIndex = 4;
            // 
            // tbDBID
            // 
            this.tbDBID.Location = new System.Drawing.Point(169, 7);
            this.tbDBID.Margin = new System.Windows.Forms.Padding(2);
            this.tbDBID.Name = "tbDBID";
            this.tbDBID.Size = new System.Drawing.Size(27, 20);
            this.tbDBID.TabIndex = 6;
            this.tbDBID.Text = "SS";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(95, 10);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Source DBID";
            // 
            // btnLog
            // 
            this.btnLog.Location = new System.Drawing.Point(398, 49);
            this.btnLog.Margin = new System.Windows.Forms.Padding(2);
            this.btnLog.Name = "btnLog";
            this.btnLog.Size = new System.Drawing.Size(20, 19);
            this.btnLog.TabIndex = 14;
            this.btnLog.Text = "...";
            this.btnLog.UseVisualStyleBackColor = true;
            this.btnLog.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 36);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Log Folder";
            // 
            // tbLog
            // 
            this.tbLog.Location = new System.Drawing.Point(35, 50);
            this.tbLog.Margin = new System.Windows.Forms.Padding(2);
            this.tbLog.Name = "tbLog";
            this.tbLog.Size = new System.Drawing.Size(368, 20);
            this.tbLog.TabIndex = 15;
            this.tbLog.Text = "C:\\temp";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.CheckFileExists = false;
            // 
            // cbcopyRec
            // 
            this.cbcopyRec.AutoSize = true;
            this.cbcopyRec.Location = new System.Drawing.Point(72, 268);
            this.cbcopyRec.Name = "cbcopyRec";
            this.cbcopyRec.Size = new System.Drawing.Size(93, 17);
            this.cbcopyRec.TabIndex = 60;
            this.cbcopyRec.Text = "Copy Records";
            this.cbcopyRec.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(238, 9);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 13);
            this.label9.TabIndex = 62;
            this.label9.Text = "Dest DBID";
            // 
            // tbDestID
            // 
            this.tbDestID.Location = new System.Drawing.Point(300, 6);
            this.tbDestID.Margin = new System.Windows.Forms.Padding(2);
            this.tbDestID.Name = "tbDestID";
            this.tbDestID.Size = new System.Drawing.Size(27, 20);
            this.tbDestID.TabIndex = 61;
            this.tbDestID.Text = "ST";
            // 
            // cbAudits
            // 
            this.cbAudits.AutoSize = true;
            this.cbAudits.Location = new System.Drawing.Point(72, 394);
            this.cbAudits.Name = "cbAudits";
            this.cbAudits.Size = new System.Drawing.Size(96, 17);
            this.cbAudits.TabIndex = 63;
            this.cbAudits.Text = "Process Audits";
            this.cbAudits.UseVisualStyleBackColor = true;
            // 
            // tbSourceServer
            // 
            this.tbSourceServer.Location = new System.Drawing.Point(93, 437);
            this.tbSourceServer.Name = "tbSourceServer";
            this.tbSourceServer.Size = new System.Drawing.Size(133, 20);
            this.tbSourceServer.TabIndex = 64;
            this.tbSourceServer.Text = "LAPTOP-APBRLN1L";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 440);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 13);
            this.label5.TabIndex = 65;
            this.label5.Text = "Source Server";
            // 
            // tbSourceDB
            // 
            this.tbSourceDB.Location = new System.Drawing.Point(318, 437);
            this.tbSourceDB.Name = "tbSourceDB";
            this.tbSourceDB.Size = new System.Drawing.Size(100, 20);
            this.tbSourceDB.TabIndex = 66;
            this.tbSourceDB.Text = "94Dev";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(257, 440);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 67;
            this.label6.Text = "Source DB";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(268, 466);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 71;
            this.label7.Text = "Dest DB";
            // 
            // tbDestDB
            // 
            this.tbDestDB.Location = new System.Drawing.Point(318, 463);
            this.tbDestDB.Name = "tbDestDB";
            this.tbDestDB.Size = new System.Drawing.Size(100, 20);
            this.tbDestDB.TabIndex = 70;
            this.tbDestDB.Text = "94Dev_New";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(27, 466);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 69;
            this.label8.Text = "Dest Server";
            // 
            // tbDestServer
            // 
            this.tbDestServer.Location = new System.Drawing.Point(93, 463);
            this.tbDestServer.Name = "tbDestServer";
            this.tbDestServer.Size = new System.Drawing.Size(133, 20);
            this.tbDestServer.TabIndex = 68;
            this.tbDestServer.Text = "LAPTOP-APBRLN1L";
            // 
            // btnCont
            // 
            this.btnCont.Location = new System.Drawing.Point(398, 84);
            this.btnCont.Margin = new System.Windows.Forms.Padding(2);
            this.btnCont.Name = "btnCont";
            this.btnCont.Size = new System.Drawing.Size(20, 19);
            this.btnCont.TabIndex = 72;
            this.btnCont.Text = "...";
            this.btnCont.UseVisualStyleBackColor = true;
            this.btnCont.Click += new System.EventHandler(this.btnCont_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(22, 72);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 13);
            this.label10.TabIndex = 74;
            this.label10.Text = "Container File";
            // 
            // tbContainers
            // 
            this.tbContainers.Location = new System.Drawing.Point(35, 86);
            this.tbContainers.Margin = new System.Windows.Forms.Padding(2);
            this.tbContainers.Name = "tbContainers";
            this.tbContainers.Size = new System.Drawing.Size(368, 20);
            this.tbContainers.TabIndex = 73;
            this.tbContainers.Text = "C:\\temp\\Containers.txt";
            // 
            // cbProcContainers
            // 
            this.cbProcContainers.AutoSize = true;
            this.cbProcContainers.Location = new System.Drawing.Point(72, 289);
            this.cbProcContainers.Name = "cbProcContainers";
            this.cbProcContainers.Size = new System.Drawing.Size(117, 17);
            this.cbProcContainers.TabIndex = 75;
            this.cbProcContainers.Text = "Process Containers";
            this.cbProcContainers.UseVisualStyleBackColor = true;
            // 
            // btnDocs
            // 
            this.btnDocs.Location = new System.Drawing.Point(398, 119);
            this.btnDocs.Margin = new System.Windows.Forms.Padding(2);
            this.btnDocs.Name = "btnDocs";
            this.btnDocs.Size = new System.Drawing.Size(20, 19);
            this.btnDocs.TabIndex = 76;
            this.btnDocs.Text = "...";
            this.btnDocs.UseVisualStyleBackColor = true;
            this.btnDocs.Click += new System.EventHandler(this.btnDocs_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(22, 106);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 13);
            this.label11.TabIndex = 78;
            this.label11.Text = "Documents File";
            // 
            // tbDocFile
            // 
            this.tbDocFile.Location = new System.Drawing.Point(35, 120);
            this.tbDocFile.Margin = new System.Windows.Forms.Padding(2);
            this.tbDocFile.Name = "tbDocFile";
            this.tbDocFile.Size = new System.Drawing.Size(368, 20);
            this.tbDocFile.TabIndex = 77;
            this.tbDocFile.Text = "C:\\temp\\DocInfo.txt";
            // 
            // cbLoadDocs
            // 
            this.cbLoadDocs.AutoSize = true;
            this.cbLoadDocs.Location = new System.Drawing.Point(72, 331);
            this.cbLoadDocs.Name = "cbLoadDocs";
            this.cbLoadDocs.Size = new System.Drawing.Size(107, 17);
            this.cbLoadDocs.TabIndex = 79;
            this.cbLoadDocs.Text = "Load Documents";
            this.cbLoadDocs.UseVisualStyleBackColor = true;
            // 
            // cbProcParts
            // 
            this.cbProcParts.AutoSize = true;
            this.cbProcParts.Location = new System.Drawing.Point(72, 310);
            this.cbProcParts.Name = "cbProcParts";
            this.cbProcParts.Size = new System.Drawing.Size(91, 17);
            this.cbProcParts.TabIndex = 80;
            this.cbProcParts.Text = "Process Parts";
            this.cbProcParts.UseVisualStyleBackColor = true;
            // 
            // cbProcVers
            // 
            this.cbProcVers.AutoSize = true;
            this.cbProcVers.Location = new System.Drawing.Point(72, 352);
            this.cbProcVers.Name = "cbProcVers";
            this.cbProcVers.Size = new System.Drawing.Size(107, 17);
            this.cbProcVers.TabIndex = 81;
            this.cbProcVers.Text = "Process Versions";
            this.cbProcVers.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(398, 156);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(20, 19);
            this.button2.TabIndex = 82;
            this.button2.Text = "...";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(22, 143);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(50, 13);
            this.label12.TabIndex = 84;
            this.label12.Text = "Parts File";
            // 
            // tbPartFile
            // 
            this.tbPartFile.Location = new System.Drawing.Point(35, 157);
            this.tbPartFile.Margin = new System.Windows.Forms.Padding(2);
            this.tbPartFile.Name = "tbPartFile";
            this.tbPartFile.Size = new System.Drawing.Size(368, 20);
            this.tbPartFile.TabIndex = 83;
            this.tbPartFile.Text = "C:\\temp\\PartInfo.txt";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(398, 193);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(20, 19);
            this.button3.TabIndex = 85;
            this.button3.Text = "...";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(22, 180);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(66, 13);
            this.label13.TabIndex = 87;
            this.label13.Text = "Versions File";
            // 
            // tbVersionFile
            // 
            this.tbVersionFile.AcceptsReturn = true;
            this.tbVersionFile.Location = new System.Drawing.Point(35, 194);
            this.tbVersionFile.Margin = new System.Windows.Forms.Padding(2);
            this.tbVersionFile.Name = "tbVersionFile";
            this.tbVersionFile.Size = new System.Drawing.Size(368, 20);
            this.tbVersionFile.TabIndex = 86;
            this.tbVersionFile.Text = "C:\\temp\\VersionInfo.txt";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(398, 229);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(20, 19);
            this.button4.TabIndex = 88;
            this.button4.Text = "...";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(22, 216);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(102, 13);
            this.label14.TabIndex = 90;
            this.label14.Text = "Access Controls File";
            // 
            // tbaccescontfile
            // 
            this.tbaccescontfile.AcceptsReturn = true;
            this.tbaccescontfile.Location = new System.Drawing.Point(35, 230);
            this.tbaccescontfile.Margin = new System.Windows.Forms.Padding(2);
            this.tbaccescontfile.Name = "tbaccescontfile";
            this.tbaccescontfile.Size = new System.Drawing.Size(368, 20);
            this.tbaccescontfile.TabIndex = 89;
            this.tbaccescontfile.Text = "C:\\temp\\AccessInfo.txt";
            // 
            // cbExtParts
            // 
            this.cbExtParts.AutoSize = true;
            this.cbExtParts.Location = new System.Drawing.Point(300, 268);
            this.cbExtParts.Name = "cbExtParts";
            this.cbExtParts.Size = new System.Drawing.Size(86, 17);
            this.cbExtParts.TabIndex = 91;
            this.cbExtParts.Text = "Extract Parts";
            this.cbExtParts.UseVisualStyleBackColor = true;
            // 
            // cbExtAcc
            // 
            this.cbExtAcc.AutoSize = true;
            this.cbExtAcc.Location = new System.Drawing.Point(300, 288);
            this.cbExtAcc.Name = "cbExtAcc";
            this.cbExtAcc.Size = new System.Drawing.Size(138, 17);
            this.cbExtAcc.TabIndex = 92;
            this.cbExtAcc.Text = "Extract Access Controls";
            this.cbExtAcc.UseVisualStyleBackColor = true;
            // 
            // cbAccCont
            // 
            this.cbAccCont.AutoSize = true;
            this.cbAccCont.Location = new System.Drawing.Point(72, 373);
            this.cbAccCont.Name = "cbAccCont";
            this.cbAccCont.Size = new System.Drawing.Size(143, 17);
            this.cbAccCont.TabIndex = 93;
            this.cbAccCont.Text = "Process Access Controls";
            this.cbAccCont.UseVisualStyleBackColor = true;
            // 
            // FrmStart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 587);
            this.Controls.Add(this.cbAccCont);
            this.Controls.Add(this.cbExtAcc);
            this.Controls.Add(this.cbExtParts);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.tbaccescontfile);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.tbVersionFile);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.tbPartFile);
            this.Controls.Add(this.cbProcVers);
            this.Controls.Add(this.cbProcParts);
            this.Controls.Add(this.cbLoadDocs);
            this.Controls.Add(this.btnDocs);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.tbDocFile);
            this.Controls.Add(this.cbProcContainers);
            this.Controls.Add(this.btnCont);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tbContainers);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbDestDB);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tbDestServer);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbSourceDB);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbSourceServer);
            this.Controls.Add(this.cbAudits);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tbDestID);
            this.Controls.Add(this.cbcopyRec);
            this.Controls.Add(this.btnLog);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbLog);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbDBID);
            this.Controls.Add(this.tbStop);
            this.Controls.Add(this.tbStart);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Name = "FrmStart";
            this.Text = "TRIM Importer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbStart;
        private System.Windows.Forms.TextBox tbStop;
        private System.Windows.Forms.TextBox tbDBID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnLog;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbLog;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.CheckBox cbcopyRec;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbDestID;
        private System.Windows.Forms.CheckBox cbAudits;
        private System.Windows.Forms.TextBox tbSourceServer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbSourceDB;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbDestDB;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbDestServer;
        private System.Windows.Forms.Button btnCont;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbContainers;
        private System.Windows.Forms.CheckBox cbProcContainers;
        private System.Windows.Forms.Button btnDocs;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbDocFile;
        private System.Windows.Forms.CheckBox cbLoadDocs;
        private System.Windows.Forms.CheckBox cbProcParts;
        private System.Windows.Forms.CheckBox cbProcVers;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbPartFile;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tbVersionFile;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbaccescontfile;
        private System.Windows.Forms.CheckBox cbExtParts;
        private System.Windows.Forms.CheckBox cbExtAcc;
        private System.Windows.Forms.CheckBox cbAccCont;
    }
}