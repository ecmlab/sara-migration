﻿using HP.HPTRIM.SDK;

namespace AWImport
{
    public static class Utils
    {
        public static Location FindLocByUri(this Database db, FieldDefinition fd, long uri)
        {
            TrimMainObjectSearch search = new TrimMainObjectSearch(db, BaseObjectTypes.Location);
            TrimSearchClause clause = new TrimSearchClause(db, BaseObjectTypes.Location, fd);
            clause.SetCriteriaFromString(uri.ToString());
            search.AddSearchClause(clause);

            foreach (Location loc in search)
                return loc;

            return null;
        }
    }
}