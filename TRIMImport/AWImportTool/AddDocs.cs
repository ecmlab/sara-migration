﻿using System;
using System.Globalization;
using System.IO;
using HP.HPTRIM.SDK;

namespace AWImport
{
    public class AddDocs : IDisposable
    {
        public void Doit(string destId, string logfile, string docFile)
        {
            var destDb = new Database { Id = destId };
            destDb.Connect();

            using (StreamReader sr = new StreamReader(docFile))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    if (line.Length > 0)
                    {
                        string[] bits = line.Split('\t');
                        // 0    1           2       3   4                                                                               5                                               6                           7                           8       9
                        // Rev	RR18/8262	710828	107	\slmnas\SARA_Doc_staging\SARA-RM8-DocStore\SS\14\001\00EX\0J081JDF07R.XLSX	    14	                                            001+00EX+0J081JDF07R.XLSX   18476	                    xlsx
                        // Main RR18/ 8262   710828  108 \slmnas\SARA_Doc_staging\SARA - RM8 - DocStore\SS\14\003\006X\0J081JGO0G5.XLSX Offline Records on 'COMP642.records.nsw.gov.au' 14                          003+006X+0J081JGO0G5.XLSX   18499   XLSX
                        if (bits[0] == "Rev")
                        {
                            // Get the record based on the SARA URI field.

                            TrimMainObjectSearch mySrch = new TrimMainObjectSearch(destDb, BaseObjectTypes.Record);
                            mySrch.SetSearchString("OldTRIMURI: " + bits[2]);

                            if (mySrch.Count == 1)
                            {
                                foreach (Record thisRec in mySrch)
                                {
                                    try
                                    {
                                        ElectronicStore newStore = null;

                                        TrimMainObjectSearch findStores =
                                            new TrimMainObjectSearch(thisRec.Database, BaseObjectTypes.ElectronicStore) { SearchString = "uri:" + bits[5] };
                                        if (findStores.Count == 1)
                                        {
                                            foreach (ElectronicStore thisStore in findStores)
                                            {
                                                newStore = thisStore;
                                            }
                                        }
                                        if (newStore != null)
                                        {
                                            //string revStoreId2 = thisRec.EStoreId;
                                            InputDocument myDoc = new InputDocument(newStore, bits[6], Convert.ToInt64(bits[7]), bits[8]);
                                            thisRec.SetDocument(myDoc, true, false, String.Empty);
                                            thisRec.Save();
                                            using (var sw = new StreamWriter(logfile, true))
                                            {
                                                sw.WriteLine(TrimDateTime.Now + "\tDocument added for URI:" + '\t' + bits[2] + '\t' + " Revision:" + '\t' + bits[3]);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        using (var sw = new StreamWriter(logfile, true))
                                        {
                                            sw.WriteLine(TrimDateTime.Now + "\tError attaching Revision document for URI:" + '\t' + bits[2] + '\t' + ex.Message);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                using (var sw = new StreamWriter(logfile, true))
                                {
                                    sw.WriteLine(TrimDateTime.Now + "\tRecord not found or more than one record found for Revision URI:" + '\t' + bits[2]);
                                }
                            }
                        }
                        else if (bits[0] == "Main")
                        {
                            // Get the record based on the SARA URI field.

                            TrimMainObjectSearch mySrch = new TrimMainObjectSearch(destDb, BaseObjectTypes.Record);
                            mySrch.SetSearchString("OldTRIMURI: " + bits[2]);

                            if (mySrch.Count == 1)
                            {
                                foreach (Record thisRec in mySrch)
                                {
                                    try
                                    {
                                        ElectronicStore newStore = null;

                                        TrimMainObjectSearch findStores =
                                            new TrimMainObjectSearch(thisRec.Database, BaseObjectTypes.ElectronicStore) { SearchString = "uri:" + bits[6] };
                                        if (findStores.Count == 1)
                                        {
                                            foreach (ElectronicStore thisStore in findStores)
                                            {
                                                newStore = thisStore;
                                            }
                                        }

                                        if (newStore != null)
                                        {
                                            InputDocument myDoc = new InputDocument(newStore, bits[7], Convert.ToInt64(bits[8]), bits[9]);
                                            thisRec.SetDocument(myDoc, false, false, String.Empty);
                                            thisRec.Save();
                                            using (var sw = new StreamWriter(logfile, true))
                                            {
                                                sw.WriteLine(TrimDateTime.Now + "\tDocument added for Main URI:" + '\t' + bits[2] + '\t' + " Revision/Main:" + '\t' + bits[3]);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        using (var sw = new StreamWriter(logfile, true))
                                        {
                                            sw.WriteLine(TrimDateTime.Now + "\tError attaching Main document for URI:" + '\t' + bits[2] + '\t' + ex.Message);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                using (var sw = new StreamWriter(logfile, true))
                                {
                                    sw.WriteLine(TrimDateTime.Now + "\tRecord not found or more than one record found for Main URI:" + '\t' + bits[2]);
                                }
                            }
                        }
                        if (bits[0] == "Rend")
                        {
                            // 0    1           2       3                                                                           4                                                                           5   6                           7       8
                            // Rend	RR16/12569	612898	C:\Users\sally\AppData\Local\Hewlett-Packard\HP TRIM\11\TEMP\npwyxop1.msg	\slmnas\SARA_Doc_staging\SARA-RM8-DocStore\SS\14\001\008X\0H0P0ICS3RSI.MSG	14	001+008X+0H0P0ICS3RSI.MSG	645632	msg
                            // Get the record based on the SARA URI field.

                            TrimMainObjectSearch mySrch = new TrimMainObjectSearch(destDb, BaseObjectTypes.Record);
                            mySrch.SetSearchString("OldTRIMURI: " + bits[2]);

                            if (mySrch.Count == 1)
                            {
                                foreach (Record thisRec in mySrch)
                                {
                                    try
                                    {
                                        thisRec.ChildRenditions.NewRendition('\\' + bits[4].Replace(@"\SS\", @"\ST\"), RenditionType.Annotation, bits[3]);
                                        
                                        thisRec.Save();
                                        using (var sw = new StreamWriter(logfile, true))
                                        {
                                            sw.WriteLine("Document added for URI:" + '\t' + bits[2] + '\t' + " Rendition:" + '\t' + bits[4]);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        using (var sw = new StreamWriter(logfile, true))
                                        {
                                            sw.WriteLine(TrimDateTime.Now + "\tError attaching Rendition document for URI:" + '\t' + bits[2] + '\t' + ex.Message);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                using (var sw = new StreamWriter(logfile, true))
                                {
                                    sw.WriteLine(TrimDateTime.Now + "\tRecord not found or more than one record found for Rendition URI:" + '\t' + bits[2]);
                                }
                            }
                        }
                    }
                }
            }
        }

        public void Dispose()
        {

        }
    }
}