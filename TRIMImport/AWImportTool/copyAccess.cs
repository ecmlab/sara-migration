﻿using System;
using System.IO;
using HP.HPTRIM.SDK;
using Enum = HP.HPTRIM.SDK.Enum;

namespace AWImport
{
    public class CopyAccess : IDisposable
    {
        public void Run(string dbid, string logfile, string accessFile)
        {
            var db = new Database { Id = dbid };
            db.Connect();

            // Get a record and try and copy by Record Type...
            TrimMainObjectSearch mySrchRecType = new TrimMainObjectSearch(db, BaseObjectTypes.RecordType);
            mySrchRecType.SetSearchString("all");

            if (mySrchRecType.Count > 0)
            {
                for (var i = 9; i > -1; i--)
                {
                    foreach (RecordType recordType in mySrchRecType)
                    {
                        if (recordType.Name == "Photocopy File" || recordType.Name == "Photocopy Document") continue;
                        if (recordType.Level == i)
                        {
                            TrimMainObjectSearch mySrch = new TrimMainObjectSearch(db, BaseObjectTypes.Record);
                            // mySrch.SetSearchString("all");
                            mySrch.SetSearchString("updated:This Year");
                            mySrch.SetFilterString("type:" + recordType.Uri);
                            mySrch.SetSortString("uri");
                            if (mySrch.Count > 0)
                            {
                                foreach (Record sourceRec in mySrch)
                                {
                                    TrimAccessControlList inAccess = sourceRec.AccessControlList;
                                    using (var sw = new StreamWriter(accessFile, true))
                                    {
                                        //sw.WriteLine("START");
                                        for (int j = 1; j < 8; j++)
                                        {
                                            if (inAccess.get_CurrentSetting(j) != AccessControlSettings.Inherited &&
                                                inAccess.get_CurrentSetting(j) != AccessControlSettings.Public)
                                            {
                                                //sw.WriteLine(sourceRec.Uri.ToString() + '\t' + j + '\t' + inAccess.GetAsString(j));
                                                foreach (Location accessLocation in sourceRec.AccessControlList.get_AccessLocations(j))
                                                {
                                                    sw.WriteLine(sourceRec.Uri.ToString() + '\t' + j + '\t' + accessLocation.Name + '\t' + accessLocation.Uri);
                                                }
                                            }
                                        }
                                        //sw.WriteLine("END");
                                    }
                                }
                            }
                        }
                    }
                }
            }
            db.Disconnect();
            db.Dispose();
            // enum  	RecordAccess {
            //RecordAccess.ViewDocument = 1, RecordAccess.ViewRecord = 2, RecordAccess.UpdateDocument = 3, RecordAccess.UpdateMetadata = 4,
            //RecordAccess.ModifyAccess = 5, RecordAccess.DestroyRecord = 6, RecordAccess.AddContents = 7
        }

        public void Dispose()
        {

        }
    }
}