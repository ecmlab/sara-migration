﻿using System;
using System.IO;
using System.Windows.Forms;
using HP.HPTRIM.SDK;

namespace AWImport
{
    public class DoParts : IDisposable
    {
        public void Dispose()
        {

        }

        public void Run(string destId, string logfile, string parts)
        {
            var destDb = new Database { Id = destId };
            destDb.Connect();

            using (StreamReader sr = new StreamReader(parts))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    if (line != string.Empty)
                    {
                        string[] bits = line.Split('\t');
                        // 0 - this part, 1 - next part

                        if (bits[0] != bits[1])
                        {
                            TrimMainObjectSearch recsrch =  new TrimMainObjectSearch(destDb, BaseObjectTypes.Record);
                            // recsrch.SetSearchString("number:" + bits[0]);
                            recsrch.SetSearchString("OldTRIMURI:" + bits[0]);

                            if (recsrch.Count == 1)
                            {
                                foreach (Record thisPart in recsrch)
                                {
                                    TrimMainObjectSearch nextParts =
                                        new TrimMainObjectSearch(destDb, BaseObjectTypes.Record);
                                    nextParts.SetSearchString("OldTRIMURI:" + bits[1]);
                                    if (nextParts.Count == 1)
                                    {
                                        foreach (Record nextPart in nextParts)
                                        {
                                            try
                                            {
                                                nextPart.AttachRelationship(thisPart, RecordRelationshipType.IsRootPart);
                                            }
                                            catch (Exception e)
                                            {
                                                using (var sw = new StreamWriter(logfile, true))
                                                {
                                                    sw.WriteLine(TrimDateTime.Now + "\tError setting Next Part set for: " + thisPart.Number + "\tAs\t" + nextPart.Number + '\t' + e.Message);
                                                }
                                            }
                                            // Save the Record
                                            Application.DoEvents();
                                            try
                                            {
                                                nextPart.Save();
                                                using (var sw = new StreamWriter(logfile, true))
                                                {
                                                    sw.WriteLine(TrimDateTime.Now + "\tNext Part set for: " + thisPart.Number + "\tAs\t" + nextPart.Number);
                                                }
                                            }
                                            catch (Exception e)
                                            {
                                                using (var sw = new StreamWriter(logfile, true))
                                                {
                                                    sw.WriteLine(TrimDateTime.Now + "\tError setting Next Part for: " +
                                                                 thisPart.Number + "\tAs\t" + nextPart.Number + '\t' +
                                                                 e.Message);
                                                }
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            destDb.Disconnect();
            destDb.Dispose();
        }
    }
}